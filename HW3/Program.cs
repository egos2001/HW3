﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

interface IPaintable
{
    void Paint();
}

internal class Figure
{
    public virtual int GetDimension()
    {
        return 0;
    }

    public virtual string GetFigureType()

    {
        return "figure";
    }
}



 class Point : Figure, IPaintable
{
    public string name;
    double x;
    double? y; 
    double? z;
    public int dimension;
    public Point(string name, double x)
    {
        this.name = name;
        this.x = x;
        this.dimension = 1;
    }
    public Point(string name, double x, double y)
    {
        this.name = name;
        this.x = x;
        this.y = y;
        this.dimension = 2;
    }
    public Point(string name, double x, double y, double z)
    {
        this.name = name;
        this.x = x;
        this.y = y;
        this.z = z;
        this.dimension = 3;
    }
    public override int GetDimension()
    {
        return this.dimension;
    }
    public override string GetFigureType()
    {
        return "Point";
    }
    public void printPoint()
    {
        Console.WriteLine(name);
    }


    public void Paint()
    {
        switch (this.dimension)
        {
            case 1:
                Console.WriteLine("Point coordinates " + this.name + ": " + this.x);
                break;
            case 2:
                Console.WriteLine("Point coordinates " + this.name + ": " + this.x + ", " + this.y);
                break;
            case 3:
                Console.WriteLine("Point coordinates " + this.name + ": " + this.x + ", " + this.y + ", " + this.z);
                break;
            default:
                Console.WriteLine("An error has occurred. There's no data about coordinates of this point.");
                break;
        }
    }
}


class TestPoint
{
   public static string GetNameWithCoordinates(Point point)
    {
        return "Point " + point.name + ": the number of coordinates: " + point.dimension;
    }

    public static void Main(string[] args)
    {
  
        Point p1 = new Point("a", 27.2);
        Console.WriteLine(GetNameWithCoordinates(p1));
        Point p2 = new Point("b", 15.56, 1.25);
        Console.WriteLine(GetNameWithCoordinates(p2));
        Point p3 = new Point("c", 80.02, 13, 1.1);
        Console.WriteLine(GetNameWithCoordinates(p3));
        p3.Paint();
        Console.WriteLine(p2.GetFigureType());
        Console.WriteLine(p3.GetDimension()); 
        Console.ReadKey();
    }
}

